const AWS = require("aws-sdk");
const shell = require("shelljs");
const client = require("mongodb").MongoClient;
require("dotenv").config();
const AWS_SDK_CONFIG = {
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
  },
  region: process.env.AWS_REGION
};
AWS.config.credentials = AWS_SDK_CONFIG.credentials;

const dbURIDev = process.env.CLARK_DB_URI_DEV.replace(
  /<DB_PASSWORD>/g,
  process.env.CLARK_DB_PWD
)
  .replace(/<DB_PORT>/g, process.env.CLARK_DB_PORT)
  .replace(/<DB_NAME>/g, process.env.CLARK_DB_NAME);

const dbURIProd = process.env.CLARK_DB_URI.replace(
  /<DB_PASSWORD>/g,
  process.env.CLARK_DB_PWD
)
  .replace(/<DB_PORT>/g, process.env.CLARK_DB_PORT)
  .replace(/<DB_NAME>/g, process.env.CLARK_DB_NAME);

const dbURITest = process.env.CLARK_DB_URI_TEST;
console.log("access key id secret ", process.env.AWS_SECRET_ACCESS_KEY);

const S3 = new AWS.S3({ region: AWS_SDK_CONFIG.region });

/**
 * 
 * Connects to mongodb and changes the file paths of learning objects in s3 from 
 *  s3://clark-prod-file-uploads-backup/${cognito Id}/${learningObject id}/${learningObject revision} to 
 * s3://clark-prod-file-uploads-backup/${cognito Id}/${learning Object cuid}/${learningObject revision}
 * by recursively moving all files under the former to the latter directory
 */
async function changeFilePaths() {
  console.log("running");
  const mongo = await client.connect(dbURIProd, { useNewUrlParser: true });
  const db = mongo.db("onion");
  const s3Bucket = 'clark-dev-file-uploads-backup' //change S3 bucket HERE!
  const userInfoArray = await db
    .collection("users")
    .find({})
    .project({ _id: 1, username: 1 })
    .toArray();

  const objects = await db
    .collection("objects")
    .find({})
    .project({ _id: 1, revision: 1, cuid: 1, authorID: 1 })
    .toArray();
  let fullS3CommandList = [];
  let deletePaths = [];
  for (let userIndex = 0; userIndex < userInfoArray.length; userIndex++) {
    for (let objectIndex = 0; objectIndex < objects.length; objectIndex++) {
      if (objects[objectIndex].authorID === userInfoArray[userIndex]._id) {
        let fileAccessIdentity = await db
          .collection("file-access-ids")
          .findOne({
            username: userInfoArray[userIndex].username
          });
        let cognitoId = fileAccessIdentity.fileAccessId;
        const deletePath = `s3://${s3Bucket}/${cognitoId}/${objects[objectIndex]._id}`;
        const oldFilePath = `s3://${s3Bucket}/${cognitoId}/${objects[objectIndex]._id}/${objects[objectIndex].version}`;
        const newFilePath = `s3://${s3Bucket}${cognitoId}/${objects[objectIndex].cuid}/${objects[objectIndex].version}`;
        const fullS3Command = `aws s3 mv ${oldFilePath} ${newFilePath} --recursive`;
        fullS3CommandList.push(fullS3Command);
        deletePaths.push(deletePath);
        /*Delete paths are not used at the moment because as S3 recursively removes all of the files 
        in each directory the empty dir
        is automatically deleted*/
      }
    }
  }
  console.log(`${fullS3CommandList.length} ${deletePaths.length}`);
  fullS3CommandList.forEach(command => {
    console.log(command);
    shell.exec(command);
    console.log("executed");
  });
  console.log('Complete');
  process.exit(0);
}

changeFilePaths()
  .then(res => console.log(res))
  .catch(err => {});

  /**
   * s3 hints....
   * 
   * `aws s3 sync s3://clark-prod-file-uploads s3://clark-dev-file-uploads-backup`
   * `aws s3 mv s3://clark-prod-file-uploads-backup/${cognitoID}/${learningObjectId}/${version}/ s3://clark-prod-file-uploads-backup/${cognitoID}/${cuid}/${version}/`
   * `aws s3 rm s3://clark-prod-file-uploads-backup/${cognitoID}/${learningObjectId}/ --recursive`
   * 
   */
